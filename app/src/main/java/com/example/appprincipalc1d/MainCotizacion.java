package com.example.appprincipalc1d;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

public class MainCotizacion extends AppCompatActivity {

    private EditText clientNameEditText;
    private Button quoteButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_cotizacion);

        clientNameEditText = findViewById(R.id.clientNameEditText);
        quoteButton = findViewById(R.id.quoteButton);

        quoteButton.setOnClickListener(this::onClick);

        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });
    }

    private void onClick(View view) {
        String clientName = clientNameEditText.getText().toString();
        if (clientName.isEmpty()) {
            Toast.makeText(getApplicationContext(), "Faltan datos por llenar", Toast.LENGTH_SHORT).show();
            return;
        }
        Intent intent = new Intent(MainCotizacion.this, com.example.appprincipalc1d.CotizacionActivity.class);
        intent.putExtra("CLIENT_NAME", clientName);
        startActivity(intent);
    }
}
